package elka.gis.graph;

import elka.gis.solver.OrientedGraphSolver;
import elka.gis.utils.PathUtils;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

public class GraphTest {
    Graph graph = new Graph(4);

    Graph graph2 = new Graph(5);

    Graph graph3 = new Graph(20);

    Graph graph4 = new Graph(6);
//
    Graph graph5 = new Graph(3);
//
    Graph graph6 = new Graph(3);
//
    Graph graph7 = new Graph(4);

    @Test
    public void vertices() {
        addVertexes(4, graph);
        assertTrue(graph.getVerticesSize() == 4);
    }

    @Test
    public void edges() {
        vertices();
        graph.addEdge(graph.getVertices().get(0), graph.getVertices().get(1), (double) 12);
        graph.addEdge(graph.getVertices().get(2), graph.getVertices().get(3), (double) 5);
        graph.addEdge(graph.getVertices().get(1), graph.getVertices().get(2), (double) 9);
        graph.addEdge(graph.getVertices().get(3), graph.getVertices().get(0), (double) 4);
        graph.addEdge(graph.getVertices().get(0), graph.getVertices().get(2), (double) 6);
        assertTrue(graph.getEdges().size() == 5);
    }

    @Test
    public void shortestPath() {
        edges();
        OrientedGraphSolver ogs1 = new OrientedGraphSolver(graph);
        assertTrue(PathUtils.getPathCost((ArrayList<Edge>) ogs1.solve()) == Double.parseDouble("45"));
    }

    @Test
    public void vertices2() {
        addVertexes(5, graph2);
        assertTrue(graph2.getVerticesSize() == 5);
    }

    @Test
    public void edges2() {
        vertices2();
        graph2.addEdge(graph2.getVertices().get(0), graph2.getVertices().get(1), (double) 12);
        graph2.addEdge(graph2.getVertices().get(1), graph2.getVertices().get(0), (double) 1);
        graph2.addEdge(graph2.getVertices().get(4), graph2.getVertices().get(1), (double) 20);
        graph2.addEdge(graph2.getVertices().get(1), graph2.getVertices().get(2), (double) 3);
        graph2.addEdge(graph2.getVertices().get(3), graph2.getVertices().get(1), (double) 2);
        graph2.addEdge(graph2.getVertices().get(2), graph2.getVertices().get(4), (double) 4);
        graph2.addEdge(graph2.getVertices().get(2), graph2.getVertices().get(3), (double) 1);
        graph2.addEdge(graph2.getVertices().get(4), graph2.getVertices().get(2), (double) 10);
        assertTrue(graph2.getEdges().size() == 8);
    }

    @Test
    public void shortestPath2() {
        edges2();
        OrientedGraphSolver ogs2 = new OrientedGraphSolver(graph2);
        assertTrue(PathUtils.getPathCost((ArrayList<Edge>)ogs2.solve()) == Double.parseDouble("60"));
    }

    @Test
    public void vertices3() {
        addVertexes(20, graph3);
        assertTrue(graph3.getVerticesSize() == 20);
    }

    @Test
    public void edges3() {
        vertices3();
        graph3.addEdge(graph3.getVertices().get(0), graph3.getVertices().get(6), (double) 11);
        graph3.addEdge(graph3.getVertices().get(0), graph3.getVertices().get(4), (double) 7);
        graph3.addEdge(graph3.getVertices().get(3), graph3.getVertices().get(0), (double) 9);
        graph3.addEdge(graph3.getVertices().get(4), graph3.getVertices().get(3), (double) 4);
        graph3.addEdge(graph3.getVertices().get(6), graph3.getVertices().get(16), (double) 55);
        graph3.addEdge(graph3.getVertices().get(16), graph3.getVertices().get(15), (double) 3);
        graph3.addEdge(graph3.getVertices().get(15), graph3.getVertices().get(10), (double) 17);
        graph3.addEdge(graph3.getVertices().get(3), graph3.getVertices().get(15), (double) 22);
        graph3.addEdge(graph3.getVertices().get(15), graph3.getVertices().get(7), (double) 4);
        graph3.addEdge(graph3.getVertices().get(7), graph3.getVertices().get(5), (double) 9);
        // graph3.addEdge(graph3.getVertices().get(5),
        // graph3.getVertices().get(6), (double) 8);
        graph3.addEdge(graph3.getVertices().get(5), graph3.getVertices().get(8), (double) 10);
        graph3.addEdge(graph3.getVertices().get(8), graph3.getVertices().get(10), (double) 12);
        graph3.addEdge(graph3.getVertices().get(10), graph3.getVertices().get(7), (double) 13);
        graph3.addEdge(graph3.getVertices().get(10), graph3.getVertices().get(14), (double) 8);
        graph3.addEdge(graph3.getVertices().get(14), graph3.getVertices().get(13), (double) 19);
        graph3.addEdge(graph3.getVertices().get(14), graph3.getVertices().get(17), (double) 3);
        graph3.addEdge(graph3.getVertices().get(13), graph3.getVertices().get(11), (double) 11);
        graph3.addEdge(graph3.getVertices().get(17), graph3.getVertices().get(18), (double) 1);
        graph3.addEdge(graph3.getVertices().get(18), graph3.getVertices().get(13), (double) 5);
        graph3.addEdge(graph3.getVertices().get(18), graph3.getVertices().get(19), (double) 4);
        graph3.addEdge(graph3.getVertices().get(19), graph3.getVertices().get(12), (double) 4);
        graph3.addEdge(graph3.getVertices().get(12), graph3.getVertices().get(9), (double) 20);
        graph3.addEdge(graph3.getVertices().get(11), graph3.getVertices().get(9), (double) 7);
        graph3.addEdge(graph3.getVertices().get(9), graph3.getVertices().get(1), (double) 14);
        graph3.addEdge(graph3.getVertices().get(9), graph3.getVertices().get(10), (double) 25);
        graph3.addEdge(graph3.getVertices().get(10), graph3.getVertices().get(2), (double) 6);
        graph3.addEdge(graph3.getVertices().get(1), graph3.getVertices().get(2), (double) 2);
        graph3.addEdge(graph3.getVertices().get(2), graph3.getVertices().get(3), (double) 5);
    }



    @Test
    public void shortestPath3() {
        edges3();
        OrientedGraphSolver ogs3 = new OrientedGraphSolver(graph3);
        assertTrue(PathUtils.getPathCost((ArrayList<Edge>)ogs3.solve()) == Double.parseDouble("418"));
    }

    @Test
    public void vertices4() {
        addVertexes(6, graph4);
        assertTrue(graph4.getVerticesSize() == 6);
    }

    @Test
    public void edges4() {
        vertices4();
        graph4.addEdge(graph4.getVertices().get(0), graph4.getVertices().get(3), (double) 5);
        graph4.addEdge(graph4.getVertices().get(3), graph4.getVertices().get(0), (double) 4);
        graph4.addEdge(graph4.getVertices().get(0), graph4.getVertices().get(4), (double) 3);
        graph4.addEdge(graph4.getVertices().get(4), graph4.getVertices().get(0), (double) 4);
        graph4.addEdge(graph4.getVertices().get(0), graph4.getVertices().get(5), (double) 2);
        graph4.addEdge(graph4.getVertices().get(5), graph4.getVertices().get(2), (double) 7);
        graph4.addEdge(graph4.getVertices().get(5), graph4.getVertices().get(1), (double) 1);
        graph4.addEdge(graph4.getVertices().get(2), graph4.getVertices().get(1), (double) 9);
        graph4.addEdge(graph4.getVertices().get(1), graph4.getVertices().get(0), (double) 3);
        assertTrue(graph4.getEdges().size() == 9);
    }

    @Test
    public void shortestPath4() {
        edges4();
        OrientedGraphSolver ogs4 = new OrientedGraphSolver(graph4);
        assertTrue(PathUtils.getPathCost((ArrayList<Edge>)ogs4.solve()) == Double.parseDouble("43"));
    }


    @Test
    public void vertices5() {
        addVertexes(3, graph5);
        assertTrue(graph5.getVerticesSize() == 3);
    }

    @Test
    public void edges5() {
        vertices5();
        graph5.addEdge(graph5.getVertices().get(0), graph5.getVertices().get(1), (double) 1);
        graph5.addEdge(graph5.getVertices().get(0), graph5.getVertices().get(2), (double) 3);
        graph5.addEdge(graph5.getVertices().get(0), graph5.getVertices().get(2), (double) 8);
        graph5.addEdge(graph5.getVertices().get(0), graph5.getVertices().get(2), (double) 50);
        graph5.addEdge(graph5.getVertices().get(1), graph5.getVertices().get(2), (double) 2);
        graph5.addEdge(graph5.getVertices().get(2), graph5.getVertices().get(0), (double) 3);

        assertTrue(graph5.getEdges().size() == 6);
    }

    @Test
    public void shortestPath5() {
        edges5();
        OrientedGraphSolver ogs5 = new OrientedGraphSolver(graph5);
        assertTrue(PathUtils.getPathCost((ArrayList<Edge>)ogs5.solve()) == Double.parseDouble("76"));
    }

    @Test
    public void vertices6() {
        addVertexes(3, graph6);
        assertTrue(graph6.getVerticesSize() == 3);
    }

    @Test
    public void edges6() {
        vertices6();
        graph6.addEdge(graph6.getVertices().get(0), graph6.getVertices().get(2), (double) 1);
        graph6.addEdge(graph6.getVertices().get(0), graph6.getVertices().get(1), (double) 1);
        graph6.addEdge(graph6.getVertices().get(0), graph6.getVertices().get(1), (double) 3);
        graph6.addEdge(graph6.getVertices().get(1), graph6.getVertices().get(0), (double) 2);
        graph6.addEdge(graph6.getVertices().get(2), graph6.getVertices().get(1), (double) 3);

        assertTrue(graph6.getEdges().size() == 5);
    }

    @Test
    public void shortestPath6() {
        edges6();
        OrientedGraphSolver ogs6 = new OrientedGraphSolver(graph6);
        assertTrue(PathUtils.getPathCost((ArrayList<Edge>)ogs6.solve()) == Double.parseDouble("14"));
    }

    @Test
    public void vertices7() {
        addVertexes(4, graph7);
        assertTrue(graph7.getVerticesSize() == 4);
    }

    @Test
    public void edges7() {
        vertices7();
        graph7.addEdge(graph7.getVertices().get(0), graph7.getVertices().get(2), (double) 1);
        graph7.addEdge(graph7.getVertices().get(2), graph7.getVertices().get(1), (double) 3);
        graph7.addEdge(graph7.getVertices().get(1), graph7.getVertices().get(0), (double) 2);
        graph7.addEdge(graph7.getVertices().get(2), graph7.getVertices().get(3), (double) 4);
        graph7.addEdge(graph7.getVertices().get(3), graph7.getVertices().get(2), (double) 4);
        graph7.addEdge(graph7.getVertices().get(3), graph7.getVertices().get(1), (double) 5);
        graph7.addEdge(graph7.getVertices().get(1), graph7.getVertices().get(3), (double) 5);
        graph7.addEdge(graph7.getVertices().get(3), graph7.getVertices().get(1), (double) 6);
        graph7.addEdge(graph7.getVertices().get(1), graph7.getVertices().get(3), (double) 6);

        assertTrue(graph7.getEdges().size() == 9);
    }

    @Test
    public void shortestPath7() {
        edges7();
        OrientedGraphSolver ogs7 = new OrientedGraphSolver(graph7);
        assertTrue(PathUtils.getPathCost((ArrayList<Edge>)ogs7.solve()) == Double.parseDouble("36"));

    }

    private void addVertexes(int number, Graph graph) {
        for (int i = 0; i < number; ++i) {
            Vertex vertex = new Vertex();
            graph.addVertex(vertex);
        }
    }

}