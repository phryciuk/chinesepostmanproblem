package elka.gis.graph;

/**
 * Created by robak on 23.03.14.
 */

public class Edge implements Cloneable {

	private Vertex begin;

	private Vertex end;

	private String name;

	private double cost;

	private boolean isDirected;

	private boolean isSAFake;

	public boolean isSAFake() {
		return isSAFake;
	}

	public void setSAFake(boolean isSAFake) {
		this.isSAFake = isSAFake;
	}

	public String getName() {
		return name;
	}

	public Edge(Vertex begin, Vertex end, double cost, boolean isDirected, String name) {
		this.begin = begin;
		this.end = end;
		this.cost = cost;
		this.isDirected = isDirected;
		this.name = name;
	}

	public Edge(Edge e) {
		this.begin = new Vertex(e.begin);
		this.end = new Vertex(e.end);
		this.cost = e.cost;
		this.isDirected = e.isDirected;
		this.name = e.name;
		this.isSAFake = e.isSAFake();
	}

	public Edge(Vertex begin, Vertex end, double cost, String name) {
		this(begin, end, cost, true, name);
	}

	public boolean isDirected() {
		return isDirected;
	}

	public void setDirected(boolean isDirected) {
		this.isDirected = isDirected;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vertex getBegin() {
		return begin;
	}

	public Vertex getEnd() {
		return end;
	}

	public double getCost() {
		return cost;
	}

	public void print() {
		System.out.println(this.getName() + " " + begin.getNumber() + " ---> " + end.getNumber() + ";");
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Edge))
			return false;

		if (((Edge) object).isDirected != this.isDirected)
			return false;

		if (!((Edge) object).name.equals(this.name))
			return false;

		if (this.isDirected && ((Edge) object).getBegin().getNumber() == this.getBegin().getNumber()
				&& ((Edge) object).getEnd().getNumber() == this.getEnd().getNumber() && ((Edge) object).getCost() == this.getCost()) {
			return true;
		}

		if (!this.isDirected
				&& ((((Edge) object).getBegin().getNumber() == this.getBegin().getNumber() && ((Edge) object).getEnd().getNumber() == this
						.getEnd().getNumber()) || (((Edge) object).getBegin().getNumber() == this.getEnd().getNumber() && ((Edge) object)
						.getEnd().getNumber() == this.getBegin().getNumber()))) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		if (isDirected)
			return begin.hashCode() - 1000 * end.hashCode();
		else {
			int grater = begin.hashCode() > end.hashCode() ? begin.hashCode() : end.hashCode();
			int lower = begin.hashCode() > end.hashCode() ? end.hashCode() : begin.hashCode();
			return grater ^ lower;
		}
	}

	@Override
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String toString() {
		return name + ": " + begin.getNumber() + " --> " + end.getNumber();
	}
}
