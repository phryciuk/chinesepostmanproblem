package elka.gis.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robak on 17.04.14.
 */
public class Graph {

	// Fields
	private List<Vertex> vertices;

	private List<Edge> edges; // *

	private final int SIZE;

	// Getters
	public int getSIZE() {
		return SIZE;
	}

	public int getVerticesSize() {
		return vertices.size();
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public List<Vertex> getVertices() {
		return vertices;
	}

	// Add methods

	public Vertex addVertex(Vertex v) {
		v.setNumber(vertices.size());
		vertices.add(v);
		return v;
	}

	public Edge addEdge(Vertex v1, Vertex v2, double cost) {
		return addEdge(v1, v2, cost, true, "edge" + edges.size());
	}

	public Edge addEdge(Vertex v1, Vertex v2, double cost, boolean isDirected) {
		return addEdge(v1, v2, cost, isDirected, "edge" + edges.size());
	}

	public Edge addEdge(Vertex v1, Vertex v2, double cost, boolean isDirected, String name) {
		if (v1 != null && v2 != null && v1 != v2) {
			v1.incrementDelta();
			v2.decrementDelta();
			Edge edge = new Edge(v1, v2, cost, isDirected, name);
			edges.add(edge);
			return edge;
		}
		return null;
	}

	public void setVertices(ArrayList<Vertex> vertices) {
		this.vertices = vertices;
	}

	public Graph(int vertices) {
		if ((SIZE = vertices) <= 0) {
			throw new Error("Graph is empty");
		}

		this.edges = new ArrayList<>();
		this.vertices = new ArrayList<>();
	}
}
