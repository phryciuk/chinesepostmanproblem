package elka.gis.graph;

/**
 * Created by robak on 23.03.14.
 */

public class Vertex {
    private int delta;
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }

    public void incrementDelta() {
        delta++;
    }

    public void decrementDelta() {
        delta--;
    }

    public Vertex () {}

    public Vertex (Vertex v) {
        this.delta = v.delta;
        this.number = v.number;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Vertex && ((Vertex) object).getNumber() == this.getNumber() && ((Vertex) object).getDelta() == this.getDelta()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return number;
    }
}