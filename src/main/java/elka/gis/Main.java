package elka.gis;

import java.io.IOException;
import java.util.ArrayList;

import elka.gis.graph.Edge;
import elka.gis.graph.Graph;
import elka.gis.heuristics.sa.SimulatedAnnealing;
import elka.gis.utils.FileUtils;
import elka.gis.utils.PathUtils;

public class Main {

	private static final String INPUT_FILE = "-f";

	private static final String OUTPUT_file = "-o";

	public static void main(String[] args) {
		int i = 0;
		String inputFile = null, outputFile = null;

		while (i < args.length && args[i].startsWith("-")) {
			String arg = args[i++];

			switch (arg) {
				case INPUT_FILE: {
					inputFile = args[i++];
				}
					break;
				case OUTPUT_file: {
					outputFile = args[i++];
				}
					break;
			}
		}

		if (null == inputFile) {
			System.err.println("Usage: mcpp.jar -f fileIn [-o fileOut]");
		} else {
			try {
				Graph graph = FileUtils.readGraphFromFile(inputFile);
				SimulatedAnnealing saSolver = new SimulatedAnnealing(graph);
				ArrayList<Edge> path = saSolver.solve();

				PathUtils.printPathAndCost(path);
				if (null != outputFile)
					FileUtils.writePathToFile(path, outputFile);
			} catch (RuntimeException | IOException e) {
				System.err.println("Internal error:");
				e.printStackTrace();
			}
		}

		System.exit(0);
	}
}
