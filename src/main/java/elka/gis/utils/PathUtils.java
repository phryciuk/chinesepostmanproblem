package elka.gis.utils;

import java.util.ArrayList;
import java.util.List;

import elka.gis.graph.Edge;
import elka.gis.graph.Vertex;

public class PathUtils {

	public static void printPathAndCost(List<Edge> path) {
		System.out.println("Path is: ");

		double cost = 0.f;
		for (Edge edge : path) {
			cost += edge.getCost();
			edge.print();
		}

		System.out.println("Cost is: " + cost);
		return;
	}

	public static double getPathCost(ArrayList<Edge> path) {
		double cost = 0.f;

		for (Edge edge : path) {
			cost += edge.getCost();
		}

		return cost;
	}

	public static void revertVerticlesDeltas(List<Vertex> verticles) {
		if (!BaseUtils.isNullOrEmpty(verticles)) {
			for (Vertex vertex : verticles)
				vertex.setDelta(0);
		}
	}

}
