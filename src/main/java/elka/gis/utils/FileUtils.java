package elka.gis.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import elka.gis.graph.Edge;
import elka.gis.graph.Graph;
import elka.gis.graph.Vertex;

public class FileUtils {

	public static Graph readGraphFromFile(String filePath) throws RuntimeException, FileNotFoundException {
		File file = new File(filePath);

		if (!file.exists()) {
			throw new RuntimeException("File " + filePath + " does not exist!");
		}

		Scanner scanner = null;

		try {
			scanner = new Scanner(file);

			Graph graph = null;
			int vertexNumber = scanner.nextInt();
			graph = new Graph(vertexNumber);
			Vertex[] vertexes = new Vertex[vertexNumber];

			for (int i = 0; i < vertexNumber; ++i) {
				Vertex vertex = new Vertex();
				graph.addVertex(vertex);
				vertexes[i] = vertex;
			}

			while (scanner.hasNext()) {
				Vertex v1 = vertexes[scanner.nextInt()];
				Vertex v2 = vertexes[scanner.nextInt()];
				double cost = scanner.nextDouble();
				boolean isDirected = scanner.nextBoolean();
				String name = scanner.next();
				graph.addEdge(v1, v2, cost, isDirected, name);
			}

			return graph;
		} catch (InputMismatchException e) {
			throw new RuntimeException("Invalid file format!");
		} finally {
			if (null != scanner)
				scanner.close();
		}
	}

	public static void writeGraphToFile(Graph graph, String filePath) throws RuntimeException, IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(filePath));

		StringBuilder graphTxt = new StringBuilder();

		graphTxt.append(graph.getSIZE());
		graphTxt.append(System.getProperty("line.separator"));

		for (Edge edge : graph.getEdges()) {
			graphTxt.append(edge.getBegin().getNumber() + " ");
			graphTxt.append(edge.getEnd().getNumber() + " ");
			graphTxt.append(String.format("%,2f", edge.getCost()) + " ");
			graphTxt.append(edge.isDirected() + " ");
			graphTxt.append(edge.getName() + " ");
			graphTxt.append(System.getProperty("line.separator"));
		}

		bw.write(graphTxt.toString());
		bw.close();
	}

	public static void writePathToFile(ArrayList<Edge> path, String filePath) throws RuntimeException, IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(filePath));

		StringBuilder graphTxt = new StringBuilder();

		graphTxt.append(PathUtils.getPathCost(path));
		graphTxt.append(System.getProperty("line.separator"));

		for (Edge edge : path) {
			graphTxt.append(edge.getBegin().getNumber() + " ");
			graphTxt.append(edge.getEnd().getNumber() + " ");
			graphTxt.append(String.format("%,2f", edge.getCost()) + " ");
			graphTxt.append(edge.isDirected() + " ");
			graphTxt.append(edge.getName() + " ");
			graphTxt.append(System.getProperty("line.separator"));
		}

		bw.write(graphTxt.toString());
		bw.close();
	}
}
