package elka.gis.utils;

import java.util.List;

public class BaseUtils {

	public static boolean isNullOrEmpty(List<? extends Object> list) {
		if (null == list || list.isEmpty())
			return true;

		return false;
	}

}
