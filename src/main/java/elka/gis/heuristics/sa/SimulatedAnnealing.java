package elka.gis.heuristics.sa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import elka.gis.graph.Edge;
import elka.gis.graph.Graph;
import elka.gis.graph.Vertex;
import elka.gis.solver.OrientedGraphSolver;
import elka.gis.tools.DijkstraAlgorithm;
import elka.gis.utils.BaseUtils;
import elka.gis.utils.PathUtils;

public class SimulatedAnnealing {

	private static final int L_VALUE = 1000;

	private static final double INIT_TEMPERATURE = 1000;

	private static final double MIN_TEMPERATURE = 5.0;

	private static final double COOLING_RATE = 0.85;

	private Graph graph;

	private Random generator;

	private int L;

	private double initTemperature;

	private double minTemperature;

	private double coolingRate;

	public SimulatedAnnealing(Graph graph) {
		this(graph, L_VALUE, INIT_TEMPERATURE, MIN_TEMPERATURE, COOLING_RATE);
	}

	public SimulatedAnnealing(Graph graph, int l, double initTemperature, double minTemperature, double coolingRate) {
		this.graph = graph;
		this.generator = new Random(System.currentTimeMillis());

		L = l;
		this.initTemperature = initTemperature;
		this.minTemperature = minTemperature;
		this.coolingRate = coolingRate;
	}

	private double cost(ArrayList<Edge> path) {
		return PathUtils.getPathCost(path);
	}

	private Graph createGraphWithFakeEdges(Graph graph) {
		Graph fakeGraph = new Graph(graph.getSIZE());
		ArrayList<Vertex> verticles = (ArrayList<Vertex>) graph.getVertices();
		PathUtils.revertVerticlesDeltas(graph.getVertices());
		fakeGraph.setVertices(verticles);

		for (Edge edge : graph.getEdges()) {
			if (edge.isDirected()) {
				Edge dirtectedEdge = fakeGraph.addEdge(edge.getBegin(), edge.getEnd(), edge.getCost(), true, edge.getName());
				dirtectedEdge.setSAFake(false);
			} else {
				Edge fakeEdge = fakeGraph.addEdge(edge.getBegin(), edge.getEnd(), edge.getCost(), true, edge.getName() + "a");
				fakeEdge.setSAFake(true);
				fakeEdge = fakeGraph.addEdge(edge.getEnd(), edge.getBegin(), edge.getCost(), true, edge.getName() + "b");
				fakeEdge.setSAFake(true);
			}
		}
		return fakeGraph;
	}

	private ArrayList<Edge> createLonger(ArrayList<Edge> current) {
		List<Edge> duplicatedEdges = findDuplicates(current);

		if (!BaseUtils.isNullOrEmpty(duplicatedEdges)) {
			Edge duplicatedEdge = duplicatedEdges.get(generator.nextInt(duplicatedEdges.size()));
			List<Integer> indexes = findAllIndexesOfElement(current, duplicatedEdge);
			int index = indexes.get(generator.nextInt(indexes.size()));
			duplicatedEdge = current.get(index);

			ArrayList<Edge> copy = new ArrayList<Edge>(graph.getEdges());
			copy.removeAll(Collections.singleton(duplicatedEdge));

			Graph fakeGraph = new Graph(graph.getSIZE());
			fakeGraph.setVertices((ArrayList<Vertex>) graph.getVertices());

			for (Edge edge : copy) {
				if (edge.isDirected()) {
					Edge dirtectedEdge = fakeGraph.addEdge(edge.getBegin(), edge.getEnd(), edge.getCost(), true, edge.getName());
					dirtectedEdge.setSAFake(false);
				} else {
					Edge fakeEdge = fakeGraph.addEdge(edge.getBegin(), edge.getEnd(), edge.getCost(), true, edge.getName() + "a");
					fakeEdge.setSAFake(true);
					fakeEdge = fakeGraph.addEdge(edge.getEnd(), edge.getBegin(), edge.getCost(), true, edge.getName() + "b");
					fakeEdge.setSAFake(true);
				}
			}

			DijkstraAlgorithm dijkstraAlgorithm = new DijkstraAlgorithm(fakeGraph);
			dijkstraAlgorithm.execute(duplicatedEdge.getBegin());
			ArrayList<Edge> alternativePath = dijkstraAlgorithm.getPath(duplicatedEdge.getEnd());
			if (null != alternativePath) {
				ArrayList<Edge> result = new ArrayList<Edge>(current);
				result.remove(index);
				result.addAll(index, revertFakeEdges(alternativePath));

				return result;
			}
		}

		return null;
	}

	private ArrayList<Edge> createShorter(ArrayList<Edge> currentPath) {
		List<Edge> duplicatedEdges = findDuplicates(currentPath);

		if (!BaseUtils.isNullOrEmpty(duplicatedEdges)) {
			Edge duplicatedEdge = duplicatedEdges.get(generator.nextInt(duplicatedEdges.size()));
			List<Integer> indexes = findAllIndexesOfElement(currentPath, duplicatedEdge);
			int index = indexes.get(generator.nextInt(indexes.size()));
			duplicatedEdge = currentPath.get(index);
			List<Edge> edges = new ArrayList<Edge>();
			edges.add(duplicatedEdge);

			for (int i = (index + 1) % currentPath.size(), j = 0; j < currentPath.size(); ++j, i = ++i % currentPath.size()) {
				Edge edge = currentPath.get(i);

				if (duplicatedEdge.getBegin().getNumber() != edge.getBegin().getNumber()) {
					edges.add(edge);
				} else {
					List<Edge> shorterPath = new ArrayList<Edge>();

					if (i > index) {
						shorterPath.addAll(currentPath.subList(0, index));
						shorterPath.addAll(currentPath.subList(i, currentPath.size()));

					} else {
						shorterPath.addAll(currentPath.subList(i, index));
					}

					if (shorterPath.containsAll(edges)) {
						return (ArrayList<Edge>) shorterPath;
					}
				}
			}
		}

		return null;
	}

	private List<Integer> findAllIndexesOfElement(List<Edge> list, Edge edge) {
		List<Integer> results = new ArrayList<Integer>();
		for (int i = 0; i < list.size(); ++i) {
			Edge currEdge = list.get(i);
			if (currEdge.equals(edge))
				results.add(i);
		}
		return results;
	}

	public List<Edge> findDuplicates(List<Edge> listContainingDuplicates) {
		final List<Edge> setToReturn = new ArrayList<Edge>();
		final Set<Edge> tmpSet = new HashSet<Edge>();

		for (Edge edge : listContainingDuplicates) {
			if (!tmpSet.add(edge)) {
				setToReturn.add(edge);
			}
		}
		return setToReturn;
	}

	private ArrayList<Edge> getInitSolution() {
		Graph fakeGraph = createGraphWithFakeEdges(graph);
		OrientedGraphSolver graphSolver = new OrientedGraphSolver(fakeGraph);
		return revertFakeEdges((ArrayList<Edge>) graphSolver.solve());
	}

	private ArrayList<Edge> getRandomNeighbour(ArrayList<Edge> current) {
		List<ArrayList<Edge>> neightbours = new ArrayList<ArrayList<Edge>>();
		neightbours.add(current);

		ArrayList<Edge> shorter = createShorter(current);
		if (null != shorter)
			neightbours.add(shorter);
		ArrayList<Edge> longer = createLonger(current);
		if (null != longer)
			neightbours.add(longer);

		return neightbours.get(generator.nextInt(neightbours.size()));
	}

	private ArrayList<Edge> revertFakeEdges(ArrayList<Edge> fakeSolution) {
		for (Edge edge : fakeSolution) {
			if (edge.isSAFake()) {
				edge.setDirected(false);
				edge.setSAFake(false);
				String currentName = edge.getName();
				edge.setName(currentName.substring(0, currentName.length() - 1));
			}
		}

		return fakeSolution;
	}

	public ArrayList<Edge> solve() {
		double currentTemp = initTemperature;
		ArrayList<Edge> currentBest = getInitSolution();
		ArrayList<Edge> globalBest = currentBest;
		double globalS = Double.MAX_VALUE;

		int L = this.L;

		while (!stop(currentTemp, currentBest)) {
			while (L-- > 0) {
				// Create new neighbour
				ArrayList<Edge> newSolution = getRandomNeighbour(currentBest);

				double fS = cost(currentBest);
				double fSi = cost(newSolution);
				double delta = fSi - fS;

				if (fSi < globalS) {
					globalBest = newSolution;
					globalS = fSi;
				}

				if (delta < 0) {
					currentBest = newSolution;
				} else {
					if (Math.exp((0 - Math.abs(delta)) / currentTemp) > Math.random()) {
						currentBest = newSolution;
					}
				}
			}

			// Cool system
			currentTemp *= coolingRate;
			L = this.L;

			System.out.println("Current best value is: " + cost(currentBest));
			System.out.println("Current global best value is: " + cost(globalBest));
		}

		return globalBest;
	}

	private boolean stop(double temp, ArrayList<Edge> currentBest) {
		// Euler cycle
		if (currentBest.size() == graph.getEdges().size())
			return true;

		if (temp <= minTemperature)
			return true;

		return false;
	}
}
