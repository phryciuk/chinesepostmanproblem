package elka.gis.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import elka.gis.graph.Edge;
import elka.gis.graph.Graph;
import elka.gis.graph.Vertex;

public class DijkstraAlgorithm {

	private final List<Edge> edges;

	private Set<Vertex> settledNodes;

	private Set<Vertex> unSettledNodes;

	private Map<Vertex, Edge> predecessors;

	private Map<Vertex, Double> distance;

	public DijkstraAlgorithm(Graph graph) {
		// create a copy of the array so that we can operate on this array
		this.edges = new ArrayList<Edge>(graph.getEdges());
	}

	public void execute(Vertex source) {
		settledNodes = new HashSet<Vertex>();
		unSettledNodes = new HashSet<Vertex>();
		distance = new HashMap<Vertex, Double>();
		predecessors = new HashMap<Vertex, Edge>();
		distance.put(source, 0.);
		unSettledNodes.add(source);
		while (unSettledNodes.size() > 0) {
			Vertex node = getMinimum(unSettledNodes);
			settledNodes.add(node);
			unSettledNodes.remove(node);
			findMinimalDistances(node);
		}
	}

	private void findMinimalDistances(Vertex node) {
		List<Edge> adjacentNodes = getNeighbors(node);
		for (Edge target : adjacentNodes) {
			if (getShortestDistance(target.getEnd()) > getShortestDistance(node) + getDistance(node, target.getEnd())) {
				distance.put(target.getEnd(), getShortestDistance(node) + getDistance(node, target.getEnd()));
				predecessors.put(target.getEnd(), target);
				unSettledNodes.add(target.getEnd());
			}
		}

	}

	private double getDistance(Vertex node, Vertex target) {
		for (Edge edge : edges) {
			if (edge.getBegin().equals(node) && edge.getEnd().equals(target)) {
				return edge.getCost();
			}
		}
		throw new RuntimeException("Should not happen");
	}

	private List<Edge> getNeighbors(Vertex node) {
		List<Edge> neighbors = new ArrayList<Edge>();
		for (Edge edge : edges) {
			if (edge.getBegin().equals(node) && !isSettled(edge.getEnd())) {
				neighbors.add(edge);
			}
		}
		return neighbors;
	}

	private Vertex getMinimum(Set<Vertex> vertexes) {
		Vertex minimum = null;
		for (Vertex vertex : vertexes) {
			if (minimum == null) {
				minimum = vertex;
			} else {
				if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
					minimum = vertex;
				}
			}
		}
		return minimum;
	}

	private boolean isSettled(Vertex vertex) {
		return settledNodes.contains(vertex);
	}

	private double getShortestDistance(Vertex destination) {
		Double d = distance.get(destination);
		if (d == null) {
			return Integer.MAX_VALUE;
		} else {
			return d;
		}
	}

	/*
	 * This method returns the path from the source to the selected target and
	 * NULL if no path exists
	 */
	public ArrayList<Edge> getPath(Vertex target) {
		ArrayList<Edge> path = new ArrayList<Edge>();
		Vertex step = target;
		// check if a path exists
		if (predecessors.get(step) == null) {
			return null;
		}

		while (predecessors.get(step) != null) {
			Edge edge = predecessors.get(step);
			path.add(edge);
			step = edge.getBegin();
		}
		// Put it into the correct order
		Collections.reverse(path);
		return path;
	}

}
