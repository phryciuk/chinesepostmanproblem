package elka.gis.tools;

import java.util.Random;

import elka.gis.graph.Graph;
import elka.gis.graph.Vertex;

public class GraphGenerator {

	public static Graph generateRandomGraph(int vertexNumber, int maxCost, double pIndirected) throws RuntimeException {
		if (1 > maxCost || 2 > vertexNumber || 0.0 > pIndirected || 1.0 < pIndirected)
			throw new RuntimeException("Invalid graph generator parameters!");

		Graph graph = new Graph(vertexNumber);

		Vertex[] vertexes = new Vertex[vertexNumber];

		for (int i = 0; i < vertexNumber; ++i) {
			Vertex vertex = new Vertex();
			graph.addVertex(vertex);
			vertexes[i] = vertex;
		}

		int lastIndex = 0;
		Vertex currVertex = vertexes[lastIndex];
		Random random = new Random();

		while (lastIndex + 1 < vertexNumber) {
			double pNew = Math.min(1.0 / ((double) lastIndex + 1.0), 1.0);
			double pExisting = 1 - pNew;
			boolean isDirected = random.nextDouble() <= pIndirected ? false : true;
			double rand = random.nextDouble();
			int randCost = random.nextInt(maxCost - 1) + 1;

			if (rand <= pExisting) {
				int randomIndex = random.nextInt(lastIndex + 1);
				Vertex newVertex = vertexes[randomIndex];
				graph.addEdge(currVertex, newVertex, randCost, isDirected);
				currVertex = newVertex;
			} else {
				graph.addEdge(currVertex, vertexes[++lastIndex], randCost, isDirected);
				currVertex = vertexes[lastIndex];
			}
		}

		boolean isDirected = random.nextDouble() <= pIndirected ? false : true;
		graph.addEdge(currVertex, vertexes[0], random.nextInt(maxCost - 1) + 1, isDirected);

		return graph;
	}
}
