package elka.gis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import elka.gis.graph.Edge;
import elka.gis.graph.Graph;
import elka.gis.heuristics.sa.SimulatedAnnealing;
import elka.gis.utils.FileUtils;
import elka.gis.utils.PathUtils;

public class ParametersTestMain {

	private static final String INPUT_FILE = "-f";

	private static final String EXPECTED_VALUE = "-e";

	private static final int L[] = { 10, 100, 1000 };

	private static final double initTemperature[] = { 100., 1000. };

	private static final double coolingRate[] = { 0.85, 0.90, 0.96, 0.98 };

	private static final int MIN_TESTS_NUMBER = 20;

	private static final double acceptedError = 0.05;

	private static final double minTemperature = 5.;

	public static void main(String[] args) {
		int i = 0;
		String inputFile = null;
		double expectedValue = 0;

		while (i < args.length && args[i].startsWith("-")) {
			String arg = args[i++];

			switch (arg) {
				case INPUT_FILE: {
					inputFile = args[i++];
				}
					break;
				case EXPECTED_VALUE: {
					expectedValue = Double.parseDouble(args[i++]);
				}
					break;
			}
		}

		if (null == inputFile || expectedValue == 0.) {
			System.err.println("Usage: mcpp.jar -f fileIn -e expectedValue");
		} else {
			try {
				List<String> resultMessages = new ArrayList<String>();

				double bestResult = Double.MAX_VALUE;

				for (int j = 0; j < L.length; ++j) {
					for (int k = 0; k < initTemperature.length; ++k) {
						for (int o = 0; o < coolingRate.length; ++o) {
							int l = L[j];
							double initTemp = initTemperature[k];
							double cR = coolingRate[o];
							double costsSum = 0.;
							int foundExpectedSolution = 0;
							int testNumber;

							for (testNumber = 0; testNumber < MIN_TESTS_NUMBER; ++testNumber) {
								Graph graph = FileUtils.readGraphFromFile(inputFile);
								SimulatedAnnealing saSolver = new SimulatedAnnealing(graph, l, initTemp, minTemperature, cR);
								ArrayList<Edge> path = saSolver.solve();
								double pathCost = PathUtils.getPathCost(path);
								costsSum += pathCost;
								bestResult = Math.min(bestResult, pathCost);

								if (expectedValue == pathCost)
									foundExpectedSolution++;
							}

							while (true) {
								Graph graph = FileUtils.readGraphFromFile(inputFile);
								SimulatedAnnealing saSolver = new SimulatedAnnealing(graph, l, initTemp, minTemperature, cR);
								ArrayList<Edge> path = saSolver.solve();

								double oldAvg = costsSum / testNumber;
								double pathCost = PathUtils.getPathCost(path);
								if (expectedValue == pathCost)
									foundExpectedSolution++;
								costsSum += pathCost;
								bestResult = Math.min(bestResult, pathCost);
								double newAvg = costsSum / ++testNumber;

								if (Math.abs(newAvg - oldAvg) / oldAvg <= acceptedError) {
									resultMessages.add("---------------------------------------");
									resultMessages.add("Parameters values: l-" + l + " initTemp-" + initTemp + " coolingRate-" + cR);
									resultMessages.add("Needed iterations: " + testNumber);
									resultMessages.add("Found expected solution in: "
											+ (((double) foundExpectedSolution) / ((double) testNumber)));
									resultMessages.add("Result avg value: " + newAvg + " Error: "
											+ (Math.abs(newAvg - expectedValue) / expectedValue));
									break;
								}
							}
						}
					}
				}

				for (String msg : resultMessages) {
					System.out.println(msg);
				}

				System.out.println("BEST RESULT: " + bestResult);
			} catch (RuntimeException | IOException e) {
				System.err.println("Internal error:");
				e.printStackTrace();
			}
		}

		System.exit(0);
	}
}
