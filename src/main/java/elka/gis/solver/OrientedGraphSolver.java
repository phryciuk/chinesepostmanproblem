package elka.gis.solver;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import elka.gis.graph.Edge;
import elka.gis.graph.Graph;
import elka.gis.graph.Vertex;

/**
 * Created by robak on 17.04.14.
 */
public class OrientedGraphSolver {

	private Graph graph;

	private List<Vertex> positiveDeltas;

	private List<Vertex> negativeDeltas;

	int arcs[][]; // adjacency matrix, counts arcs between vertices

	private int[][] repeatedEdges;

	int path[][]; // spanning tree of the graph

	private double[][] distances;

	private boolean hasNegativeCycle;

	private String cheapestLabel[][];

	private Vector label[][];

	private static final int NONE = -1;

	public OrientedGraphSolver(Graph graph) {
		this.graph = graph;
		negativeDeltas = new ArrayList<>();
		positiveDeltas = new ArrayList<>();
		distances = new double[graph.getSIZE()][graph.getSIZE()];
		hasNegativeCycle = false;
		path = new int[graph.getSIZE()][graph.getSIZE()];
		arcs = new int[graph.getSIZE()][graph.getSIZE()];
		repeatedEdges = new int[graph.getSIZE()][graph.getSIZE()];
		cheapestLabel = new String[graph.getSIZE()][graph.getSIZE()];
		label = new Vector[graph.getSIZE()][graph.getSIZE()];
		initializeInfinity();
		initializeEdgesInfo();
	}

	/**
	 * Initializes distances between vertices with positive infinity
	 */
	private void initializeInfinity() {
		for (int v = 0; v < graph.getSIZE(); v++) {
			for (int w = 0; w < graph.getSIZE(); w++) {
				distances[v][w] = Double.POSITIVE_INFINITY;
			}
		}
	}

	private void initializeEdgesInfo() {
		for (Edge e : graph.getEdges()) {
			if (distances[e.getBegin().getNumber()][e.getEnd().getNumber()] == Double.POSITIVE_INFINITY) {
				label[e.getBegin().getNumber()][e.getEnd().getNumber()] = new Vector();
			}
			label[e.getBegin().getNumber()][e.getEnd().getNumber()].addElement(e.getName());

			if (distances[e.getBegin().getNumber()][e.getEnd().getNumber()] == Double.POSITIVE_INFINITY
					|| distances[e.getBegin().getNumber()][e.getEnd().getNumber()] > e.getCost()) {
				distances[e.getBegin().getNumber()][e.getEnd().getNumber()] = e.getCost();
				cheapestLabel[e.getBegin().getNumber()][e.getEnd().getNumber()] = e.getName();
				path[e.getBegin().getNumber()][e.getEnd().getNumber()] = e.getEnd().getNumber();
			}
			arcs[e.getBegin().getNumber()][e.getEnd().getNumber()]++;
		}

	}

	private void floydWarshall() {
		for (int i = 0; i < graph.getVerticesSize(); i++) {
			for (int v = 0; v < graph.getVerticesSize(); v++) {
				if (distances[v][i] == Double.POSITIVE_INFINITY)
					continue;
				for (int w = 0; w < graph.getVerticesSize(); w++) {
					if (distances[v][w] > distances[v][i] + distances[i][w]) {
						path[v][w] = path[v][i];
						distances[v][w] = distances[v][i] + distances[i][w];
					}
				}
				// check for negative cycle
				if (distances[v][v] < 0.0) {
					hasNegativeCycle = true;
					return; // stop on negative cycle
				}
			}
		}
	}

	private void checkValidity() {
		for (int i = 0; i < graph.getSIZE(); i++) {
			for (int j = 0; j < graph.getSIZE(); j++) {
				if (distances[i][j] == Double.POSITIVE_INFINITY) {
					throw new Error("Graph is not strongly connected");
				}
				if (hasNegativeCycle) {
					throw new Error("Graph has a negative cycle.");
				}
			}
		}
	}

	public List<Edge> solve() {
		printDistancesArray();
		floydWarshall();
		printDistancesArray();
		checkValidity();
		findUnbalancedVertices();
		findFeasible();
		ArrayList<Edge> solution = (ArrayList<Edge>) printSolution(graph.getEdges().get(0).getBegin().getNumber());
		return solution;
	}

	private void printDistancesArray() {
		System.out.println("Distances between vertices:");
		for (int v = 0; v < graph.getVerticesSize(); v++) {
			System.out.printf("%3d: ", v);
			for (int w = 0; w < graph.getVerticesSize(); w++) {
				if (hasPath(v, w))
					System.out.printf("%6.2f ", dist(v, w));
				else
					System.out.printf("  Inf ");
			}
			System.out.println();
		}
		System.out.println("\n");
	}

	/**
	 * Determines whether path between provided vertices exist (not only direct
	 * edges)
	 * 
	 * @param s
	 *            index of a begin vertex
	 * @param t
	 *            index of a end vertex
	 * @return Does vertices have path? True/False
	 */
	private boolean hasPath(int s, int t) {
		return distances[s][t] < Double.POSITIVE_INFINITY;
	}

	/**
	 * Returns distance between two vertices (provided by indeces)
	 * 
	 * @param s
	 *            Index of a begin vertex
	 * @param t
	 *            Index of a end vertex
	 * @return Distance between two vertices
	 */
	private double dist(int s, int t) {
		return distances[s][t];
	}

	private void findUnbalancedVertices() {
		for (Vertex v : graph.getVertices()) {
			if (v.getDelta() < 0) {
				negativeDeltas.add(v);
			} else if (v.getDelta() > 0) {
				positiveDeltas.add(v);
			}
		}
	}

	private void findFeasible() {
		for (Vertex nv : negativeDeltas) {
			for (Vertex pv : positiveDeltas) {
				if (-(nv.getDelta()) < pv.getDelta()) {
					repeatedEdges[nv.getNumber()][pv.getNumber()] = -nv.getDelta();
				} else {
					repeatedEdges[nv.getNumber()][pv.getNumber()] = pv.getDelta();
				}
				nv.setDelta(nv.getDelta() + repeatedEdges[nv.getNumber()][pv.getNumber()]);
				pv.setDelta(pv.getDelta() - repeatedEdges[nv.getNumber()][pv.getNumber()]);
                pv.getDelta();
			}
		}
	}

	private List<Edge> printSolution(int startVertex) {
		int cost = 0;
		List<Edge> solution = new ArrayList<>();
		int v = startVertex;
		int arcs[][] = new int[graph.getSIZE()][graph.getSIZE()];
		int repeatedEdges[][] = new int[graph.getSIZE()][graph.getSIZE()];

		for (int i = 0; i < graph.getSIZE(); i++) {
			for (int j = 0; j < graph.getSIZE(); j++) {
				arcs[i][j] = this.arcs[i][j];
				repeatedEdges[i][j] = this.repeatedEdges[i][j];
			}
		}

		while (true) {
			int u = v;
			if ((v = findPath(u, repeatedEdges)) != NONE) {
				repeatedEdges[u][v]--; // remove path
				for (int p; u != v; u = p) {
					p = path[u][v];
					System.out.println("Take arc " + cheapestLabel[u][p] + " from " + u + " to " + p);
					cost += findEdgeByName(cheapestLabel[u][p]).getCost();
					solution.add(findEdgeByName(cheapestLabel[u][p]));
				}
			} else {
				int bridgeVertex = path[u][startVertex];
				if (arcs[u][bridgeVertex] == 0) {
					break; // finished if bridge already used
				}
				v = bridgeVertex;
				// find an unused arc, using bridge last
				for (int i = 0; i < graph.getSIZE(); i++) {
					if (i != bridgeVertex && arcs[u][i] > 0) {
						v = i;
						break;
					}
				}
				arcs[u][v]--; // decrement count of parallel arcs
				// use each arc label in turn
				System.out.println("Take arc " + label[u][v].elementAt(arcs[u][v]) + " from " + u + " to " + v);
				cost += findEdgeByName(label[u][v].elementAt(arcs[u][v]).toString()).getCost();
				solution.add(findEdgeByName(label[u][v].elementAt(arcs[u][v]).toString()));
			}
		}
		System.out.println("Final cost is: " + cost);
		return solution;
	}

	private Edge findEdgeByName(String name) {
		for (Edge e : graph.getEdges()) {
			if (e.getName().equals(name)) {
				return e;
			}
		}
		return null;
	}

	// find a path between unbalanced vertices
	private int findPath(int from, int repeatedEdges[][]) {
		for (int i = 0; i < graph.getSIZE(); i++) {
			if (repeatedEdges[from][i] > 0) {
				return i;
			}
		}
		return NONE;
	}
}
